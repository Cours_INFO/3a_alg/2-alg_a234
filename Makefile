LIB = lib/
SRC = src/

all: a234

OPTIONS_COMPIL=-Wall -g -c

a234: a234.o lire_affichage.o ajout_cle.o file.o pile.o
	gcc -o a234 a234.o lire_affichage.o ajout_cle.o file.o pile.o

file.o: $(addprefix $(SRC), file.c) $(addprefix $(LIB), file.h) $(addprefix $(LIB), a234.h)
	gcc $(OPTIONS_COMPIL) $(addprefix $(SRC), file.c)

pile.o: $(addprefix $(SRC), pile.c) $(addprefix $(LIB), pile.h) $(addprefix $(LIB), a234.h)
	gcc $(OPTIONS_COMPIL) $(addprefix $(SRC), pile.c)

a234.o: $(addprefix $(SRC), a234.c) $(addprefix $(LIB), a234.h) $(addprefix $(LIB), file.h) $(addprefix $(LIB), pile.h) 
	gcc $(OPTIONS_COMPIL) $(addprefix $(SRC), a234.c)

lire_affichage.o: $(addprefix $(SRC), lire_affichage.c) $(addprefix $(LIB), a234.h)
	gcc $(OPTIONS_COMPIL) $(addprefix $(SRC), lire_affichage.c)

ajout_cle.o : $(addprefix $(SRC), ajout_cle.c) $(addprefix $(LIB), a234.h)
	gcc $(OPTIONS_COMPIL) $(addprefix $(SRC), ajout_cle.c)

clean:
	rm -rf a234 *.o *~
