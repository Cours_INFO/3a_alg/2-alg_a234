# TP2 - A234 : Arbre 234

Travail Pratique dans le cas du cours d'Algorithmique Avancée à **POLYTECH GRENOBLE**.

***

## **TABLE DES MATIÈRES**

* **[Sujet](./TP2ALG.pdf)**

* **[Travail](#travail)**

***

## **TRAVAIL**

### **1. Préparation du TP**

Prendre l'archive [A234.tar.gz](./archive/A234.tar.gz)

### **2. Arbres A234**

### **Liste des fonctions dans le fichier a234:**

| fonctions dans le fichier a234 | Descriptions |
|:------------------------------:|:------------:|
| `int hauteur (Arbre234 a);` | utilise simplement un parcours en profondeur et ajoute 1 lors du return. |
| `int NombreCles (Arbre234 a);` | utilise simplement un parcours en profondeur et ajoute le nombre de clé de chaque fils. |
| `int CleMax (Arbre234 a);` | accède au fils le plus à droite accessible jusqu'à atteindre une feuille et renvoie la clé la plus grande du nœud. |
| `int CleMin (Arbre234 a);` | accède au fils le plus à gauche accessible jusqu'à atteindre une feuille et renvoie la clé la plus petite du nœud. |
| `Arbre234 RechercherCle (Arbre234 a, int cle);` | cherche la clé en se déplaçant dans l’arbre et renvoie le noeud où il y a la clé. |
| `void AnalyseStructureArbre (Arbre234 a, int *feuilles, int *noeud2, int *noeud3, int *noeud4);` | fait un parcours en largeur et incrémente les valeurs contenues dans les pointeur en fonction du type de nœud. |
| `Arbre234 noeud_max (Arbre234 a);` | fait un parcours en profondeur et fait la somme des clés du noeud et les compare avec ces fils, le max renvoie le nœud. |
| `void Afficher_Cles_Largeur (Arbre234 a);` | utilise la structure file pour stocker les nœuds, tout d’abord parcourt l’ensemble des nœuds contenue dans la file en les affichant puis on ajoute les nœuds fils dans la file. |
| `void Affichage_Cles_Triees_Recursive (Arbre234 a);` | utilise un parcours en profondeur classique. |
| `void Affichage_Cles_Triees_NonRecursive (Arbre234 a);` | utilise une structure pile qui conserve un noeud et un entier. |

Le principe est de chercher le minimum et d'ajouter à la pile chaque nœud parcouru avec un entier à 0, ensuite on remonte en autant de la pile et en incrémentant l’entier de 1.

Si le noeud a encore des fils inexplorée, on l’ajoute dans la pile avec le nouvel entier.

#### **fonctions utilisées pour détruire clé:**

| fonctions utilisées pour détruire clé |
|:------------------------------:|
| `void Detruire_Cle (Arbre234 *a, int cle);` |
| `Arbre234 cherche(Arbre234 a,int cle,ppile_t pile);` |
| `Arbre234 remonter(Arbre234 a, ppile_t pile);` |
| `Arbre234 reequilibragegauche(Arbre234 parent,int i);` |
| `Arbre234 reequilibragedroite(Arbre234 parent ,int i);` |
| `Arbre234 reequilibremerge(Arbre234 parent,int i);` |
| `Arbre234 rotationg(Arbre234 parent,Arbre234 a,int i);` |
| `Arbre234 rotationd(Arbre234 parent,Arbre234 a,int i);` |
| `Arbre234 merge(Arbre234 parent,int i);` |
| `Arbre234 leaf(Arbre234 a,int cle,ppile_t pile);` |
| `Arbre234 findmin(Arbre234 a);` |
| `Arbre234 swap(Arbre234 a,Arbre234 min,int i);` |
| `Arbre234 merge_1_1_1(Arbre234 a);` |
| `Arbre234 cherche(Arbre234 a,int cle,ppile_t pile);` |



### **Destruction clé**

Lorsque l’on veut supprimer une clé, il y a plusieurs cas possibles. En effet, il faut d’abord chercher la clé avec la fonction `cherche()`, une fois trouvé il y a plusieurs cas possible traiter dans la fonction `found()`:
- cas 1: la clé est dans une feuille
- cas 2: la clé est dans un noeud interne

#### **Traitement du cas 1:**

On arrive dans la fonction leaf et plusieurs sous cas sont possibles:
- cas 1.1: le noeud est un 3,4
- cas 1.2: le noeud est un 2 mais son frère gauche ou droit est 3,4
- cas 1.3:le noeud est un 2 mais ni  son frère gauche ou droit est 3,4
    - cas 1.3.1: le parent du noeud est 3,4
    - cas 1.3.2: le parent du noeud est 2

#### **Traitement du cas 1.1:**

On a juste à réduire le noeud, il est traité directement dans leaf

#### **Traitement du cas 1.2:**

On appelle la fonction `rotationg()` ou rotationd pour passer une clé de parent au nœud 2 et une clé d’un des frères au parent.

#### **Traitement du cas 1.3:**

- cas 1.3.1: le parent du noeud est 3,4
- cas 1.3.2: le parent du noeud est 2

#### **Traitement du cas 1.3.1:**

On utilise la fonction merge avec la fonction `merge()` qui réduit le nœud parent et passe une de ses clés à un des fils.

#### **Traitement du cas 1.3.2:**

On crée un déséquilibre en faisant une fusion du parent et de l’autre fils. On tente de rééquilibrer l’arbre avec la fonction `remonter()`. Ainsi, on monte en hauteur de 1 et pour rééquilibrer il y a 3 cas:
- cas 1.3.2.1: l’un des frères du neoud est un 3,4
- cas 1.3.2.1: aucun des frères du neoud est un 3,4 mais le parent est un 3,4
- cas 1.3.2.1: ni le parent, ni le frère est un 3,4

#### **Traitement du cas 1.3.2.1:**

On fait appelle à la fonction `rééquilibregauche()` ou `rééquilibragedroite()` pour modifier la hauteur en faisant remonter une clé d’un frère et en créant un nouveau nœud entre le parent et celui déséquilibrer la clé du nouveau nœud est une des clés de parent. De plus en second fils le nouveau nœud reçoit un nœud fils d’un frère. 

#### **Traitement du cas 1.3.2.2:**

On fait appelle à la fonction `rééquilibremerge()` qui diminue le nœud parent de 1 pour descendre l’une des clé dans un nœud fils équilibré et reçoit dans pour un des ses fils le nœud déséquilibré.

#### **Traitement du cas 1.3.2.3:**

On fait un rééquilibrage locale de l’arbre mais on doit de nouveaux faire appel à la fonction `remonter` pour rééquilibrer globalement l’arbre.

#### **Traitement du cas 2:**

On cherche la plus petite clé du fils droit de la clé avec `findmin()` et on échange les deux clés avec la fonction `swap()`. Ainsi, on se retrouve dans le cas 1.

<u>Remarque</u>:

Le fait de détruire une clé implique beaucoup de sous cas possibles entraînant de nombreuses difficultés et la nécessité de garder un arbre équilibré.
